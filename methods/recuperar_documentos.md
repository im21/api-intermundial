## Recuperación de documentos

Descripcion

```
GET /v1/es/insurances.json/documents/:documentId
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Codigo de lenguaje segun el standar [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento (json,xml)|
| documentId | string | Id del documento que se quiere descargar|


****
**Datos de la solicitud**

```json
{   "url": "https://api.intermundial.com/v1/es/insurances.json/documents/2788089",
    "method": "GET",
    "headers": {
        "AuthToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded",
     }
}
```


****
**Cabeceras de respuesta con éxito**

Al hacer la solicitud el servidor devolvera el fichero como una descarga

```
Accept-Ranges:bytes
Access-Control-Allow-Origin:*
Cache-Control:public, max-age=2592000
Connection:Keep-Alive
Content-Length:800083
Content-Type:application/pdf
Date:Tue, 15 Nov 2016 14:34:34 GMT
Expires:Thu, 15 Dec 2016 14:34:34 GMT
Keep-Alive:timeout=5, max=100
Last-Modified:Tue, 15 Nov 2016 14:34:40 GMT
Server:Apache/2.4.6 (CentOS) OpenSSL/1.0.1e-fips
Strict-Transport-Security:max-age=31536000; preload
```