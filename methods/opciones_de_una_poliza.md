## Opciones de una póliza

Llamada que devuelve el conjunto de las opciones de contratación que tiene una póliza: destino, duración, modalidad, ampliaciones, etc.

```
GET /v1/:_locale/rate/insurances.:_format/:npoliza
```


****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml]|
| npoliza | string | Número de póliza |


****
**Datos de la solicitud**

```json
{
  "url": "https://api.intermundial.com/v1/es/rate/insurances.json/MMC-PRUEBA",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
    "cache-control": "no-cache"    
  }
}
```


****
**Respuesta con éxito**

De entre todas las propiedades que contiene el objeto devuelto, destacamos:
* RESULT->CONTENT[0]->IDTARIFA => Código de identificacion del tarificador.
* RESULT->CONTENT[0]->PARAMS[0]->IDPARAM => Código de identificación del parámetro.
* RESULT->CONTENT[0]->PARAMS[0]->VALORES[n]->IDVAL => Código de identificación de los posibles valores de cada parámetro.
* RESULT->CONTENT[0]->TIPOTARIFA => Tipo de tarificador, fundamental para construir el objeto XML con la estructura apropiada
* RESULT->CONTENT[0]->INFOCOMERCIAL => Información comercial del producto.
* RESULT->CONTENT[0]->INFOCOMERCIAL->INFOCOMERCIAL1 => Valor mínimo de producto.
* RESULT->CONTENT[0]->TARIFAPLANA => Si su valor 0, quiere decir que el importe del seguro se debe multiplicar por el numero de
pasajeros, si su valor es 1, quiere decir que el importe del seguro es unico para cualquier cantidad de pasajeros.


```json
{
  HTTP_STATUS 200,
  CODE "OK",
  MESSAGE "OK",
  RESPONSE_FORMAT "JSON",
  RESULT {
    CONTENT [
      {
  --->  IDTARIFA "601",
        DESCRIPCION "PRUEBA",
  --->  TARIFAPLANA "0",
        IDDIVISA "0",
        INFOCOMERCIAL {
  --->    INFOCOMERCIAL1 "25.6",
  --->    INFOCOMERCIAL2 null
        },
        PARAMS [
          {
  --->      IDPARAM "605",
            NUMPARAM "1",
            DESCRIPCION "Destino",
            TIPODATO "S",
            DESTIPODATO null,
            TBLAUX null,
  --->      VALORES [
              {
                IDVAL "1462",
                DESCRIPCION "Mundo",
                NDESDE "0",
                NHASTA "0"
              },
              {
                IDVAL "1461",
                DESCRIPCION "Europa",
                NDESDE "0",
                NHASTA "0"
              }
            ]
          },
          {
  --->      IDPARAM "606",
            NUMPARAM "2",
            DESCRIPCION "Duración",
            TIPODATO "R",
            DESTIPODATO "días",
            TBLAUX null,
  --->      VALORES [
              {
                IDVAL "1463",
                DESCRIPCION "De 1 a 34 días",
                NDESDE "1",
                NHASTA "34"
              },
              {
                IDVAL "1464",
                DESCRIPCION "De 35 a 68 días",
                NDESDE "35",
                NHASTA "68"
              }
            ]
          }
        ],
  --->  GARANTIAS [
          {
            ID "100000020908",
            IDGARANTIA "0",
            NOMBREGARANTIA "ASISTENCIA",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "001",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023809",
            IDGARANTIA "10219",
            NOMBREGARANTIA "Gastos Médicos",
            SUMAASEGURADA "0",
            OBSERVACIONES "Asistencia médica y sanitaria",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023810",
            IDGARANTIA "9014",
            NOMBREGARANTIA "Gastos Odontológicos",
            SUMAASEGURADA "120",
            OBSERVACIONES "Gastos Odontológicos",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023811",
            IDGARANTIA "10160",
            NOMBREGARANTIA "Desplazamiento de un familiar por hospitalizacion",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Gastos por desplazamiento de un familiar por hospitalizacion del asegurado superior a 5 días",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "003",
              NIVEL "2"
            }
          },
          {
            ID "100000023812",
            IDGARANTIA "8813",
            NOMBREGARANTIA "Gastos de estancia del acompañante",
            SUMAASEGURADA "1200",
            OBSERVACIONES "Gastos de estancia del acompañante desplazado (max 120e dia)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "004",
              NIVEL "2"
            }
          },
          {
            ID "100000023813",
            IDGARANTIA "8585",
            NOMBREGARANTIA "Prolongación de estancia en hotel",
            SUMAASEGURADA "1200",
            OBSERVACIONES "Prolongación de estancia en hotel por prescripción médica (max 120€ dia)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "005",
              NIVEL "2"
            }
          },
          {
            ID "100000023814",
            IDGARANTIA "9251",
            NOMBREGARANTIA "Repatriacion o transporte por enfermedad o Acciden",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Repatriacion o transporte sanitario de heridos o enfermos",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "006",
              NIVEL "2"
            }
          },
          {
            ID "100000023815",
            IDGARANTIA "8814",
            NOMBREGARANTIA "Repatriación o transporte del fallecido",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Repatriación o transporte del fallecido",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "007",
              NIVEL "2"
            }
          },
          {
            ID "100000023816",
            IDGARANTIA "8815",
            NOMBREGARANTIA "Repatriacion de acompañantes asegurados",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Repatriacion de acompañantes asegurados",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "008",
              NIVEL "2"
            }
          },
          {
            ID "100000023817",
            IDGARANTIA "10271",
            NOMBREGARANTIA "Repatriación de menores o disminuidos",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Repatriación de menores o disminuidos",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "009",
              NIVEL "2"
            }
          },
          {
            ID "100000023818",
            IDGARANTIA "8851",
            NOMBREGARANTIA "Servicio de Intérprete",
            SUMAASEGURADA "0",
            OBSERVACIONES "Servicio de Intérprete",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "010",
              NIVEL "2"
            }
          },
          {
            ID "100000023819",
            IDGARANTIA "9114",
            NOMBREGARANTIA "Envio de medicamentos no existentes en el extranje",
            SUMAASEGURADA "0",
            OBSERVACIONES "Envio de medicamentos no existentes en el extranjero",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "011",
              NIVEL "2"
            }
          },
          {
            ID "100000023820",
            IDGARANTIA "8853",
            NOMBREGARANTIA "Adelanto de fondos monetarios",
            SUMAASEGURADA "2500",
            OBSERVACIONES "Adelanto de fondos monetarios",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "012",
              NIVEL "2"
            }
          },
          {
            ID "100000023821",
            IDGARANTIA "10272",
            NOMBREGARANTIA "Transmisión de mensajes urgentes",
            SUMAASEGURADA "0",
            OBSERVACIONES "Transmisión de mensajes urgentes",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "013",
              NIVEL "2"
            }
          },
          {
            ID "100000023822",
            IDGARANTIA "9080",
            NOMBREGARANTIA "Apertura y reparación de cofres y cajas de segurid",
            SUMAASEGURADA "150",
            OBSERVACIONES "Apertura y reparación de cofres y cajas de seguridad",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "014",
              NIVEL "2"
            }
          },
          {
            ID "100000023823",
            IDGARANTIA "8553",
            NOMBREGARANTIA "Pérdida de llaves de la vivienda habitual",
            SUMAASEGURADA "60",
            OBSERVACIONES "Pérdida de llaves de la vivienda habitual",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "015",
              NIVEL "2"
            }
          },
          {
            ID "100000023824",
            IDGARANTIA "9327",
            NOMBREGARANTIA "Gtos anul tarjetas robadas o perdidas",
            SUMAASEGURADA "0",
            OBSERVACIONES "Gastos de anulacion de tarjetas robadas o perdidas",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "016",
              NIVEL "2"
            }
          },
          {
            ID "100000023825",
            IDGARANTIA "10274",
            NOMBREGARANTIA "Asesoramiento juridico telefonico",
            SUMAASEGURADA "0",
            OBSERVACIONES "Asesoramiento juridico telefonico",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "017",
              NIVEL "2"
            }
          },
          {
            ID "100000023826",
            IDGARANTIA "10275",
            NOMBREGARANTIA "Información general",
            SUMAASEGURADA "0",
            OBSERVACIONES "Información general",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "018",
              NIVEL "2"
            }
          },
          {
            ID "100000023827",
            IDGARANTIA "10276",
            NOMBREGARANTIA "Transporte alternativo por accidente in-itinere",
            SUMAASEGURADA "350",
            OBSERVACIONES "Transporte alternativo por accidente in-itinere",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "019",
              NIVEL "2"
            }
          },
          {
            ID "100000023855",
            IDGARANTIA "10381",
            NOMBREGARANTIA "Deportes",
            SUMAASEGURADA "0",
            OBSERVACIONES "Deportes",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "001",
              POSICION "020",
              NIVEL "2"
            }
          },
          {
            ID "100000023828",
            IDGARANTIA "0",
            NOMBREGARANTIA "PROTECCION JURIDICA",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "002",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023829",
            IDGARANTIA "10277",
            NOMBREGARANTIA "Reclamacion de daños en el extranjero",
            SUMAASEGURADA "3000",
            OBSERVACIONES "Reclamacion de daños en el extranjero",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "002",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023830",
            IDGARANTIA "10278",
            NOMBREGARANTIA "Reclamacion de contratos de compra en el Extranjero",
            SUMAASEGURADA "3000",
            OBSERVACIONES "Reclamacion de contratos de compra en el Extranjero",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "002",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023831",
            IDGARANTIA "10279",
            NOMBREGARANTIA "Reclamacion de contratos de servicios en el extranjero",
            SUMAASEGURADA "3000",
            OBSERVACIONES "Reclamacion de contratos de servicios en el extranjero",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "002",
              POSICION "003",
              NIVEL "2"
            }
          },
          {
            ID "100000023832",
            IDGARANTIA "10261",
            NOMBREGARANTIA "Adelanto de fianzas",
            SUMAASEGURADA "9000",
            OBSERVACIONES "Adelanto de fianzas en el extranjero",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "002",
              POSICION "004",
              NIVEL "2"
            }
          },
          {
            ID "100000023833",
            IDGARANTIA "0",
            NOMBREGARANTIA "EQUIPAJES",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "003",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023834",
            IDGARANTIA "10257",
            NOMBREGARANTIA "Robo, perdida y daños materiales de equipaje",
            SUMAASEGURADA "0",
            OBSERVACIONES "Robo, perdida y daños materiales de equipaje",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "003",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023835",
            IDGARANTIA "8994",
            NOMBREGARANTIA "Demora en la entrega de equipaje facturado",
            SUMAASEGURADA "350",
            OBSERVACIONES "Gastos ocasionados por la demora en la entrega de equipaje facturado (150€ a partir de 12 horas y 100€ cada hora)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "003",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023836",
            IDGARANTIA "8817",
            NOMBREGARANTIA "Búsqueda, localización y envío de equipaje",
            SUMAASEGURADA "0",
            OBSERVACIONES "Búsqueda, localización y envío de equipaje",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "003",
              POSICION "003",
              NIVEL "2"
            }
          },
          {
            ID "100000023837",
            IDGARANTIA "8986",
            NOMBREGARANTIA "Envio de doc perdida/ robada en viaje",
            SUMAASEGURADA "125",
            OBSERVACIONES "Envio de doc perdida/ robados en viaje",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "003",
              POSICION "004",
              NIVEL "2"
            }
          },
          {
            ID "100000023838",
            IDGARANTIA "0",
            NOMBREGARANTIA "GASTOS DE ANULACION",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "004",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023839",
            IDGARANTIA "8034",
            NOMBREGARANTIA "Gastos de anulación de viaje",
            SUMAASEGURADA "0",
            OBSERVACIONES "Gastos de anulación de viaje",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "004",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023840",
            IDGARANTIA "8964",
            NOMBREGARANTIA "Reembolso de vacaciones no disfrutadas",
            SUMAASEGURADA "0",
            OBSERVACIONES "Reembolso de vacaciones no disfrutadas",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "004",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023841",
            IDGARANTIA "0",
            NOMBREGARANTIA "DEMORAS Y PERDIDAS DE SERVICIOS",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "005",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023842",
            IDGARANTIA "9530",
            NOMBREGARANTIA "Salida de un transporte alternativo",
            SUMAASEGURADA "330",
            OBSERVACIONES "Gastos ocasionados por la salida de un medio de transporte alternativo no previsto (55€ cada 6 horas)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023843",
            IDGARANTIA "10287",
            NOMBREGARANTIA "Cambio de hotel/apartamento",
            SUMAASEGURADA "550",
            OBSERVACIONES "Gastos ocasionados por el cambio de hotel/apartamento (max 55€ dia)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023844",
            IDGARANTIA "9031",
            NOMBREGARANTIA "Perdida de servicios contratados",
            SUMAASEGURADA "300",
            OBSERVACIONES "Perdida de servicios inicialmente contratados",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "003",
              NIVEL "2"
            }
          },
          {
            ID "100000023845",
            IDGARANTIA "8821",
            NOMBREGARANTIA "Regreso anticipado del viaje fallec u hosp. familiar",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Regreso anticipado del viaje por fallecimiento u hospitalización de un familiar",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "004",
              NIVEL "2"
            }
          },
          {
            ID "100000023846",
            IDGARANTIA "10289",
            NOMBREGARANTIA "Regreso anticipado por siniestro grave en el hogar o local",
            SUMAASEGURADA "1000000",
            OBSERVACIONES "Regreso anticipado por siniestro grave en el hogar o local profesional",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "005",
              NIVEL "2"
            }
          },
          {
            ID "100000023847",
            IDGARANTIA "10326",
            NOMBREGARANTIA "Perdida de conexiones",
            SUMAASEGURADA "750",
            OBSERVACIONES "Perdida de conexiones (minimo 4 horas de retraso)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "006",
              NIVEL "2"
            }
          },
          {
            ID "100000023848",
            IDGARANTIA "10164",
            NOMBREGARANTIA "Demora salida medio transporte",
            SUMAASEGURADA "350",
            OBSERVACIONES "Gastos ocasionados por la demora en la salida del medio de transporte (50€ a partir de 6 horas y 100€ cada 24 horas adiccionales)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "007",
              NIVEL "2"
            }
          },
          {
            ID "100000023849",
            IDGARANTIA "8966",
            NOMBREGARANTIA "Extensión de viaje obligada",
            SUMAASEGURADA "300",
            OBSERVACIONES "Extensión de viaje obligada (max 75€ dia)",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "005",
              POSICION "008",
              NIVEL "2"
            }
          },
          {
            ID "100000023850",
            IDGARANTIA "0",
            NOMBREGARANTIA "ACCIDENTES",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "006",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023851",
            IDGARANTIA "9156",
            NOMBREGARANTIA "Indemnización por fallec. o invalidez x acci en vi",
            SUMAASEGURADA "10000",
            OBSERVACIONES "Indemnización por fallecimiento o invalidez permanente del asegurado por accidente en el viaje 24 horas",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "006",
              POSICION "001",
              NIVEL "2"
            }
          },
          {
            ID "100000023852",
            IDGARANTIA "9179",
            NOMBREGARANTIA "Indemnización por fallecimi o invalidez en el medio de trans",
            SUMAASEGURADA "35000",
            OBSERVACIONES "Indemnización por fallecimiento o invalidez del asegurados por accidente en el medio de transporte",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "006",
              POSICION "002",
              NIVEL "2"
            }
          },
          {
            ID "100000023853",
            IDGARANTIA "0",
            NOMBREGARANTIA "RESPONSABILIDAD CIVIL",
            SUMAASEGURADA "0",
            OBSERVACIONES null,
            CLASIFICACION "GRUPO",
            GRUPO {
              CODIGO "007",
              POSICION null,
              NIVEL "1"
            }
          },
          {
            ID "100000023854",
            IDGARANTIA "8070",
            NOMBREGARANTIA "Responsabilidad civil",
            SUMAASEGURADA "65000",
            OBSERVACIONES "Responsabilidad civil privada",
            CLASIFICACION "GARANTIA",
            GRUPO {
              CODIGO "007",
              POSICION "001",
              NIVEL "2"
            }
          }
        ],
        POLIZAS [
          {
            IDPOLIZA "15319",
            NUMPOLIZA "MMC-PRUEBA",
            RIESGODESC "PRUEBA",
            IDDIVISA "0"
          }
        ],
  --->  TIPOTARIFA "DESTINO_DURACION"
      }
    ]
  }
}
```