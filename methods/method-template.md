## Inicio de Sesión

Descripcion

```
POST /v1/:_locale/auth.:_format
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Codigo de lenguaje segun el standar [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento (json,xml)|


****
**Parámetros POST**

| Name | Type | Required |Description |
| --- | --- | --- | --- |
| user | string | si | Nombre del usuario |
| pass | string | si | Contraseña del usuario |
| type | string | si | Tipo de usuario (partner) |


****
**Datos de la solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/auth.json",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "user": "myUser",
    "pass": "myPass",
    "type": "partner"
  }
}
```


****
**Respuesta con éxito**

```json
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": {
      "ID": "22",
      "USER": "47525135M",      
      "LASTLOGINDATE": "31/08/2016 16:11:00",
      "MODIFIEDDATE": null,
      "CREATEDDATE": null,
      "STATUS": 1,
      "DELETED": 0,
      "ROLES": "1,5",
      "ACCESSIBLERESOURCES": [],
      "SESSIONKEYVALIDTIME": 1472654460,
      "SESSIONKEY": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWjPVg",
      "ENVIROMENT": "prod",
      "USERTYPE": "partner",
      "IDCLIENTE": "1197568"
    }
  }
}
```