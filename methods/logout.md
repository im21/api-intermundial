## Fin de Sesión

Llamada para cerrar la sesión de un usuario cuyos ID y token de sesión pasamos por URL y parámetro del método DELETE respectivamente.


```
DELETE /v1/:_locale/auth.:_format/:userId
```


****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml]|
| userId | string | ID de usuario recuperado en el [inicio de sesión](http://bitbucket.intermundial.xxi:7990/projects/DOC/repos/api-intermundial/browse/methods/login.md) |


****
**Datos de la Solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/auth.json/16",
  "method": "DELETE",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  }
}
```



