## Contratación de seguros

Llamada para contratar un seguro o conjunto de seguros


```
POST /v1/:_locale/partners/load/bulk.:_format
```


****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml]|


****
**Parámetros POST**

| Name | Type | Required |Description |
| --- | --- | --- | --- |
| xml | xml | si | Conjunto de seguros para dar de alta |


****
**Datos XML**

| Name | Type | Required |Description |
| --- | --- | --- | --- |
| STR_NPOLIZA | string | si | Número de póliza |
| STR_LOCALIZADOR | string | si | Identificador único del alta (para uso interno de la agencia)   |
| STR_DESTINO | string | si | Destino de la póliza |
| STR_DURACION_IMPORTE | string | si | Tipo tarificación aplicada |
| STR_MODALIDAD | string | si | Modalidad el viaje  |
| DEC_IMPORTE_RESERVA | string | si | Importe asegurado (Precio del viaje y/o alojamiento)  |
| INT_PAX | string | si | Total de asegurados |
| DEC_IMPORTE_A_VALIDAR | integer | no | Se compara el importe calculado para el seguro en este campo. Luego se valida que coincida con el importe calculado de la tarifa |
| STR_PAIS_ORIGEN | string | no | Pais de origen |
| STR_PAIS_DESTINO | string | no | Pais de destino |
| DATE_FECHA_EFECTO | string | si | Fecha de inicio del viaje |
| DATE_FECHA_BAJA | string | si | Fecha de fin del viaje |
| OBJ_ASEGURADO | xml | si | Información del asegurado principal |
| ARR_PASAJEROS | xml | no | Listado de asegurados dependientes |


****
**Ejemplo de estructura XML**

Esta es la estructura del XML a enviar en la llamada de contratación:

```xml
<OBJ_COMUNICACION>
      <ARR_SEGUROS>
          <OBJ_SEGURO>
              <STR_ACCION>ALTA</STR_ACCION>
              <STR_NOMBRE_SEGURO></STR_NOMBRE_SEGURO>
              <STR_NPOLIZA>MMC-PRUEBA</STR_NPOLIZA>
              <STR_LOCALIZADOR>interm-4</STR_LOCALIZADOR>
```

Tarificador Tipo DESTINO_DURACION
```xml
              <STR_DESTINO>Mundo</STR_DESTINO>
              <STR_DURACION_IMPORTE>DESTINO_DURACION</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>         
```

Tarificador Tipo MODALIDAD_IMPORTE
```xml
              <STR_DURACION_IMPORTE>MODALIDAD_IMPORTE</STR_DURACION_IMPORTE>
              <STR_MODALIDAD>Viaje Combinado</STR_MODALIDAD>            
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>            
              <DEC_IMPORTE_RESERVA>5000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo PORCENTAJE
```xml
              <STR_DURACION_IMPORTE>PORCENTAJE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo IMPORTE
```xml
              <STR_DURACION_IMPORTE>IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>300</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo DESTINO_IMPORTE
```xml
              <STR_DESTINO>Mundo</STR_DESTINO>
              <STR_DURACION_IMPORTE>DESTINO_IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo DURACION_IMPORTE
```xml
              <STR_DURACION_IMPORTE>DURACION_IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo EDAD_DURACION
```xml
              <STR_DURACION_IMPORTE>EDAD_DURACION</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>              
              ...
              <!-- Es importante que en este tipo de tarifa se indique la fecha de nacimiento del asegurado -->
              <OBJ_ASEGURADO>
                    <STR_NOMBRE_APELLIDOS>Fernandez Pérez, Roberto</STR_NOMBRE_APELLIDOS>
                    <STR_NIF></STR_NIF>
                    <STR_PASAPORTE></STR_PASAPORTE>
                    <DATE_FECHA_NACIMIENTO>1984-05-15</DATE_FECHA_NACIMIENTO>
                    ...
              <OBJ_ASEGURADO>
              ...
```

Tarificador Tipo PRIMAFIJA
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

En el caso de que queramamos validar que el importe que hemos calculado es el correcto tendremos que proporcionar despues de la etiqueta `<INT_PAX>` la etiqueta `<DEC_IMPORTE_A_VALIDAR>`
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DEC_IMPORTE_A_VALIDAR>12.75</DEC_IMPORTE_A_VALIDAR>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

Tambien, de manera opcional y si se quisiera especificar el pais de origen y de destino se puede hacer con las etiquetas `<STR_PAIS_ORIGEN>` y `<STR_PAIS_DESTINO>`
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DEC_IMPORTE_A_VALIDAR>12.75</DEC_IMPORTE_A_VALIDAR>
              <STR_PAIS_ORIGEN>España</STR_PAIS_ORIGEN>
              <STR_PAIS_DESTINO>Italia</STR_PAIS_DESTINO>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

Común para todos los tipos de tarificador
```xml
              <OBJ_ASEGURADO>
                  <STR_NOMBRE_APELLIDOS>Fernandez Pérez, Roberto</STR_NOMBRE_APELLIDOS>
                  <STR_NIF></STR_NIF>
                  <STR_PASAPORTE></STR_PASAPORTE>
                  <DATE_FECHA_NACIMIENTO></DATE_FECHA_NACIMIENTO>
                  <STR_SEXO></STR_SEXO>
                  <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                  <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                  <STR_BARRIO></STR_BARRIO>
                  <STR_LOCALIDAD></STR_LOCALIDAD>
                  <STR_PROVINCIA></STR_PROVINCIA>
                  <STR_CP></STR_CP>
                  <STR_PAIS></STR_PAIS>
                  <STR_EMAIL></STR_EMAIL>
                  <STR_TELEFONO></STR_TELEFONO>
                  <STR_EXTENSION></STR_EXTENSION>
              </OBJ_ASEGURADO>

              <ARR_PASAJEROS>
                  <OBJ_PASAJERO>
                      <STR_NOMBRE_APELLIDOS>De las Torres, Luis</STR_NOMBRE_APELLIDOS>
                      <STR_NIF></STR_NIF>
                      <STR_PASAPORTE></STR_PASAPORTE>
                      <DATE_FECHA_NACIMIENTO>1967-03-20</DATE_FECHA_NACIMIENTO>
                      <STR_SEXO></STR_SEXO>
                      <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                      <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                      <STR_BARRIO></STR_BARRIO>
                      <STR_LOCALIDAD></STR_LOCALIDAD>
                      <STR_PROVINCIA></STR_PROVINCIA>
                      <STR_CP></STR_CP>
                      <STR_EMAIL></STR_EMAIL>
                      <STR_TELEFONO></STR_TELEFONO>
                  </OBJ_PASAJERO>
                  <OBJ_PASAJERO>
                      <STR_NOMBRE_APELLIDOS>De las Torres, Roberto</STR_NOMBRE_APELLIDOS>
                      <STR_NIF></STR_NIF>
                      <STR_PASAPORTE></STR_PASAPORTE>
                      <DATE_FECHA_NACIMIENTO></DATE_FECHA_NACIMIENTO>
                      <STR_SEXO></STR_SEXO>
                      <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                      <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                      <STR_BARRIO></STR_BARRIO>
                      <STR_LOCALIDAD></STR_LOCALIDAD>
                      <STR_PROVINCIA></STR_PROVINCIA>
                      <STR_CP></STR_CP>
                      <STR_EMAIL></STR_EMAIL>
                      <STR_TELEFONO></STR_TELEFONO>
                  </OBJ_PASAJERO>
              </ARR_PASAJEROS>

          </OBJ_SEGURO>
      </ARR_SEGUROS>
  </OBJ_COMUNICACION>
```


****
**Datos de la Solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/partners/load/bulk.json",
  "method": "POST",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {    
      <OBJ_COMUNICACION>
        .......
      </OBJ_COMUNICACION>"
  }
}
```


****
**Respuesta con éxito**

```json
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": {
      "SEGURO0": {
        "HTTP_STATUS": 200,
        "CODE": "OK",
        "MESSAGE": "OK",
        "RESPONSE_FORMAT": "JSON",
        "RESULT": {
          "CONTENT": {
            "IDVIAJE": "00000153190000000006",
            "IDPOLIZA": "15319",
            "NUMPOLIZA": "MMC-PRUEBA",
            "LOCALIZADOR": "interm-4",
            "FECHAINICIOVIAJE": "20/09/2025",
            "FECHAFINVIAJE": "25/09/2025",
            "COSTEVIAJE": null,
            "NPAX": "3",
            "AGENCIANIF": "B81577231",
            "AGENCIAID": null,
            "CERTIFICADO2": null,
            "PRIMATOTAL": "38.25",
            "IDTARIFA": "601",
            "PARAMS": [
              {
                "IDPARAM": "605",
                "NUMPARAM": "1",
                "VALOR": {
                  "IDVAL": "1461",
                  "DESCRIPCION": "Europa"
                }
              },
              {
                "IDPARAM": "606",
                "NUMPARAM": "2",
                "VALOR": {
                  "IDVAL": "1463",
                  "DESCRIPCION": "De 1 a 34 días"
                }
              }
            ],
            "ASEGURADOS": [
              {
                "IDASEGURADO": "6",
                "IDASEGURADODEP": null,
                "IDCLIENTE": "16878103",
                "NOMBRE": "Fernandez Pérez, Roberto",
                "TIPODOC": "NIF",
                "NDOCUMENTO": null,
                "DOMICILIO": null,
                "CPO": null,
                "LOCALIDAD": null,
                "PROVINCIA": null,
                "TELMOVIL": null,
                "EMAIL": null,
                "IDBILLETE": "000001531900000000060016878103"
              },
              {
                "IDASEGURADO": null,
                "IDASEGURADODEP": "108103",
                "IDCLIENTE": "16878104",
                "NOMBRE": "De las Torres, Luis",
                "TIPODOC": "NIF",
                "NDOCUMENTO": null,
                "DOMICILIO": null,
                "CPO": null,
                "LOCALIDAD": null,
                "PROVINCIA": null,
                "TELMOVIL": null,
                "EMAIL": null,
                "IDBILLETE": "000001531900000000000016878104"
              },
              {
                "IDASEGURADO": null,
                "IDASEGURADODEP": "108104",
                "IDCLIENTE": "16878105",
                "NOMBRE": "De las Torres, Roberto",
                "TIPODOC": "NIF",
                "NDOCUMENTO": null,
                "DOMICILIO": null,
                "CPO": null,
                "LOCALIDAD": null,
                "PROVINCIA": null,
                "TELMOVIL": null,
                "EMAIL": null,
                "IDBILLETE": "000001531900000000000016878105"
              }
            ],
            "RECIBOS": [
              {
                "IDRECIBO": "1600829116",
                "NUMRECIBO": null,
                "FECHAEFECTO": "20/09/2025",
                "FECHAVENCIMIENTO": "25/09/2025",
                "TIPO": "Suplemento",
                "SECUENCIA": "1/1",
                "RIESGODESC": "Fernandez Pérez, Roberto",
                "ESTADO": "0",
                "ESTADODESC": "GESTION COBRO",
                "REVISADO": "1",
                "REMESADO": "0",
                "IMPORTETOTAL": "38.25"
              }
            ],
            "CERTIFICADO": {
              "ORIGINALMIME": "application/x-empty",
              "ENCODEDMIME": null,
              "MD5HASH": null,
              "FILE": null
            }
          }
        }
      }
    }
  }
}
```