## Anulación de seguros

Llamada para anular un seguro o conjunto de seguros


```
DELETE /v1/:_locale/partners/load/bulk.:_format
```


****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml]|


****
**Parámetros POST**

| Name | Type | Required |Description |
| --- | --- | --- | --- |
| xml | xml | si | Conjunto de seguros para dar de alta |


****
**Datos XML**

| Name | Type | Required |Description |
| --- | --- | --- | --- |
| STR_USUARIO | string | si | Nombre del usuario |
| STR_IDVIAJE | string | si | Identificador único de la reserva |
| STR_ACCION | string | si | Acción a realizar |
| STR_NPOLIZA | string | si | Número de póliza |


****
**Ejemplo de estructura XML**

```xml
<OBJ_COMUNICACION>
    <ARR_DATOSAGENCIA>
        <STR_USUARIO></STR_USUARIO>
    </ARR_DATOSAGENCIA>
    <ARR_SEGUROS>
        <OBJ_SEGURO>
            <STR_ACCION>BAJA</STR_ACCION>
            <STR_IDVIAJE>00000153190000000023</STR_IDVIAJE>
            <STR_NPOLIZA>MMC-PRUEBA</STR_NPOLIZA>
        </OBJ_SEGURO>
    </ARR_SEGUROS>
</OBJ_COMUNICACION>
```


****
**Datos de la Solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/partners/load/bulk.json",
  "method": "POST",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {    
      "xml": "
        <OBJ_COMUNICACION>
            <ARR_DATOSAGENCIA>
                <STR_USUARIO></STR_USUARIO>
            </ARR_DATOSAGENCIA>
            <ARR_SEGUROS>
                <OBJ_SEGURO>
                    <STR_ACCION>BAJA</STR_ACCION>
                    <STR_IDVIAJE>00000153190000000023</STR_IDVIAJE>
                    <STR_NPOLIZA>MMC-PRUEBA</STR_NPOLIZA>
                </OBJ_SEGURO>
            </ARR_SEGUROS>
        </OBJ_COMUNICACION>
      "
  }
}
```

**Respuesta con exito**

```json
{
    HTTP_STATUS: 200,
    CODE: "OK",
    MESSAGE: "OK",
    RESPONSE_FORMAT: "JSON",
    RESULT: {
        CONTENT: {
            SEGURO0: {
                HTTP_STATUS: 200,
                CODE: "OK",
                MESSAGE: "OK",
                RESPONSE_FORMAT: "JSON",
                RESULT: {
                    CONTENT: {
                        ANULLED: true
                    }
                }
            }
        }
    }
}
```