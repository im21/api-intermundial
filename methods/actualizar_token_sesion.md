## Actualizar el token de sesion

El token de autenticación(SESSIONKEY) que devuelve la llamada de login, tan solo tiene un tiempo de validez de 30 minutos.
Para evitar tener que realizar login cada vez que se quiera hacer una llamada a la API, se ha implementado este metodo de refresco de tokens.

```
POST /v1/:_locale/auth.:_format/refreshsessiontoken
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml] |

****
**Datos de la Solicitud**

```
{
  "url": "https://secure-aleon.intermundial.com/api/v1/es/auth.json/refreshsessiontoken",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
    "cache-control": "no-cache"
  }
```


****
**Respuesta con éxito**

Unicamente se devuelve un nuevo `SESSIONKEY` con una validez de dos horas mas desde el momento actual.

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: {
      SESSIONKEY: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0Nzc0OTc5NTQsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6Ijl2ZmZpbHJkNnFyOWM4bDloN3Yxb2llcWszIiwidXNlcklkIjoiMjE4NTUifQ.U6Krs7daSlOHa2IkdLmjKPG3RLbfCQbUqi9rA8B-po4"
    }
  }
}
```