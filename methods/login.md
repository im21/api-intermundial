## Inicio de Sesión

Llamada que devuelve un objeto con información sobre el usuario y la sesión que se inicia.

```
POST /v1/:_locale/auth.:_format
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml] |


****
**Parámetros POST**

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| user | string | si | Nombre del usuario |
| pass | string | si | Contraseña del usuario |


****
**Datos de la Solicitud**

```
{  
  "url": "https://api.intermundial.com/v1/es/auth.json",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "user": "sandbox",
    "pass": "sandbox_intermundial"
  }
}
```


****
**Respuesta con éxito**

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* RESULT->CONTENT->ID => Código de identificacion del usuario.
* RESULT->CONTENT->SESSIONKEY => Token que necesitaremos en las posteriores llamadas como cabecera "AuthToken".

```json
{
    HTTP_STATUS: 200,
    CODE: "OK",
    MESSAGE: "OK",
    RESPONSE_FORMAT: "JSON",
    RESULT: {
        CONTENT: {
  --->      ID: "20939",
            LASTLOGINDATE: [],
            MODIFIEDDATE: [],
            CREATEDDATE: [],
            STATUS: "Activo",
            DELETED: 0,
            ROLES: "1,3,4",
            ACCESSIBLERESOURCES: [
              "InterMundial\WebServiceBundle\Controller\Api\AuthController::postAuthAction",
              "InterMundial\WebServiceBundle\Controller\Api\AuthController::deleteAuthAction",
              "InterMundial\WebServiceBundle\Controller\Api\AuthController::getAuthAction",
              "InterMundial\WebServiceBundle\Controller\Api\RateInsurancesController::getRateInsurancesAction",
              "InterMundial\WebServiceBundle\Controller\Api\RateInsurancesController::getRateInsurancesValuesAction",
              "InterMundial\WebServiceBundle\Controller\Api\LoadController::postLoadAction",
              "InterMundial\WebServiceBundle\Controller\Api\LoadController::putLoadAction",
              "InterMundial\WebServiceBundle\Controller\Api\LoadController::deleteLoadAction",
              "InterMundial\WebServiceBundle\Controller\Api\CertificateController::getCertificateByIdViajeAction",
              "InterMundial\WebServiceBundle\Controller\Api\ClaimsController::getClaimsAction",
              "InterMundial\WebServiceBundle\Controller\Api\FinancialController::getDolarTurismoAction",
              "InterMundial\WebServiceBundle\Controller\Api\AccountController::putAccountAction",
              "InterMundial\WebServiceBundle\Controller\Api\RateInsurancesController::getInsurancesInfoAction",
              "InterMundial\WebServiceBundle\Controller\Api\CertificateController::getCertificateByLocalizadorAction",
              "InterMundial\WebServiceBundle\Controller\Api\CertificateController::getDatosReservaByNPolizaLocalizadorAction",
              "InterMundial\WebServiceBundle\Controller\Api\RateInsurancesController::getInsuranceByTravelIdAction",
              "InterMundial\WebServiceBundle\Controller\Tools\AreasManagementController::getInsuranceScopeByCountriesAction",
              "InterMundial\WebServiceBundle\Controller\Tools\AreasManagementController::getCountriesAction",
              "InterMundial\WebServiceBundle\Controller\Api\ExternalComunicationController::postExternalComunicationAction",
              "InterMundial\WebServiceBundle\Controller\Tools\AreasManagementController::getInsuranceStretchesByStartAndEndDateAction"
            ],
            SESSIONKEYVALIDTIME: 1476341630,
  --->      SESSIONKEY: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzYzNDE2MzAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdqY2lhazFzbXNtOWhmMHIxYjM1N2czaTEyIiwidXNlcklkIjoiMjA5MzkifQ.gVC11xqk0wiUhGjQPv03Qxp13ckKOsf4qFDVPKDu1nc",
            ENVIROMENT: "prod",
            USERTYPE: "Colaborador",
            USER: "sandbox",
            PASS: "f8d3c5b1b61dadec0f724125842874b0411943fc",
            NOMBRECOMPLETO: "Prueba para los ejemplos de la documentación del WS",
            TRATAMIENTO: null,
            NOMBRE: null,
            APELLIDOS: null,
            TIPODOC: null,
            NDOCUMENTO: null,
            FNACIMIENTO: null,
            PAIS: null,
            PROVINCIA: null,
            LOCALIDAD: null,
            DOMICILIO: null,
            CP: null,
            EMAIL: "autobox@intermundial.es",
            MOVIL: null
        }
    }
}
```