## Tarificar una póliza

Devuelve un objeto con la tarifa elegida, el valor de la prima y las posibles garantías adicionales disponibles en base a los parámetros seleccionados.

```
GET /v1/:_locale/rate/:tarifaId/insurances._format?values=[:IDVAL1,:IDVAL2,...]
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| \_locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| tarifaId | string | Identificador de la tarifa \( RESULT-&gt;CONTENT-&gt;IDTARIFA \) |
| \_format | string | Formato del salida del documento [json, xml] |
| values | array | Array de opciones del objeto tarificador asociado a la póliza obtenido previamente (Destino, Duraci&oacute;n, Modalidad, ...) (RESULT-&gt;CONTENT-&gt;PARAMS-&gt;VALORES-&gt;IDVAL \| RESULT-&gt;CONTENT-&gt;PARAMS-&gt;VALORES-&gt;TBLAUX) |

Las opciones de la póliza seleccionada deben estar ordenadas por el valor 
RESULT-&gt;CONTENT-&gt;PARAMS-&gt;NUMPARAM en el array `values`.


****
### Tipos de garantias

#### 1. Garantias incluidas no ampliables

Se da en aquellos seguros que solo tienen garantias que están incluidas en las condiciones generales de póliza. Estas garantias no se pueden ampliar y por lo tanto, cuando se retorna el precio, no se devuelven en el array de garantias, puesto que ya están incluidas en el precio base del seguro.

Podemos observar que RESULT->CONTENT[0]->GARANTIAS se encuentra vacio.

Por lo tanto el precio total del seguro seria: 33.278025 €

**Datos de la solicitud (1)**
```json
{
  "url": "https://api.intermundial.com/v1/es/rate/622/insurances.json?values=[627]",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI3MzI0NzYsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InJvZWFqcnVncm91YjQ4aWQ4a",
    "cache-control": "no-cache",
  }
}
```


****
**Respuesta con éxito (1)**

```json
{  
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": [
      {
        "IDTARIFA": "622",
        "IDTARIFAPER": null,
        "PRIMATOTAL": 33.278025,
        "COEFICIENTEIMPUESTOS": "0.0615",
        "GARANTIAS": []
      }
    ]
  }
}
```

#### 2. Garantias incluidas ampliables

Se da en aquellos seguros que ademas de tener incluidas las condiciones generales de la póliza, dichas condiciones pueden ser ampliadas por coberturas mas altas de las incluidas inicialmente en la póliza. Por ejemplo podemos ampliar las cobertura de gastos médicos de `50.000 €` a `100.000 €`.

Si nos fijamos en RESULT->CONTENT[0]->GARANTIAS[0]->INCLUIDA nos indica que su valor es `1`, esto quiere decir que está cobertura viene incluida en el precio base de la pòliza. Ademas, al ser devuelta en la llamada de tarificación, nos está indicando que es una garantia ampliable.

Vamos a continuar analizando la respuesta. Vemos ademas que RESULT->CONTENT[0]->GARANTIAS[0]->SUMASASEGURADASPREDEFINIDAS nos devuelve un listado de rangos. Estos rangos son las diferentes opciones de ampliación de las garantias. Si nos fijamos en RESULT->CONTENT[0]->GARANTIAS[0]->SUMASASEGURADASPREDEFINIDAS[0]->SELPORDEFECTO vemos que su valor vale `1`. Esto quiere decir que el rango RESULT->CONTENT[0]->GARANTIAS[0]->SUMASASEGURADASPREDEFINIDAS[0] seria el rango incluido en el precio base de la póliza.

En el caso de que quiseramos ampliar hasta `100.000 €` de gastos médicos el precio final seria:

Precio Final = `RESULT->CONTENT[0]->PRIMATOTAL` + `RESULT->CONTENT[0]->GARANTIAS[0]->SUMASASEGURADASPREDEFINIDAS[1]->PRIMA`

Precio Final = `12` + `25` = `37 €`

**Datos de la solicitud (2)**
```json
{
  "url": "https://api.intermundial.com/v1/es/rate/601/insurances.json?values=[1461,1463]",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI3MzI0NzYsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InJvZWFqcnVncm91YjQ4aWQ4a",
    "cache-control": "no-cache",
  }
}
```


****
**Respuesta con éxito (2)**

```json
{
  HTTP_STATUS 200,
  CODE "OK",
  MESSAGE "OK",
  RESPONSE_FORMAT "JSON",
  RESULT {
    CONTENT [
      {
        IDTARIFA "601",
        IDTARIFAPER "764",
        PRIMATOTAL 12,
        COEFICIENTEIMPUESTOS "0.0615",
        GARANTIAS [
          {
            ID "200000021597",
            IDGARANTIA "10219",
            IDGARANTIATARIFA "21597",
            IDGARANTIARAMO null,
            NOMBREGARANTIA "Gastos Médicos",
            INCLUIDA "1",
            DESINCLUIDA "Si",
            SUMAASEGURADATIPO "2",
            DESSUMAASEGURADATIPO "Lista",
            SUMAASEGURADAMIN null,
            SUMAASEGURADAMAX null,
            TBLGARANTIA null,
            SUMASASEGURADASPREDEFINIDAS [
              {
                SELPORDEFECTO "1",
                SUMAASEGURADA "50000",
                PRIMA "0",
                IDTARIFALST "1208",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "100000",
                PRIMA "25",
                IDTARIFALST "1214",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "150000",
                PRIMA "50",
                IDTARIFALST "1209",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "200000",
                PRIMA "75",
                IDTARIFALST "1210",
                IDRANGOTBL null
              }
            ]
          },
          {
            ID "200000021641",
            IDGARANTIA "10381",
            IDGARANTIATARIFA "21641",
            IDGARANTIARAMO null,
            NOMBREGARANTIA "Deportes",
            INCLUIDA "2",
            DESINCLUIDA "Opcional",
            SUMAASEGURADATIPO "2",
            DESSUMAASEGURADATIPO "Lista",
            SUMAASEGURADAMIN null,
            SUMAASEGURADAMAX null,
            TBLGARANTIA null,
            SUMASASEGURADASPREDEFINIDAS [
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "1000",
                PRIMA "1",
                IDTARIFALST "1202",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "2000",
                PRIMA "2",
                IDTARIFALST "1203",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "3000",
                PRIMA "3",
                IDTARIFALST "1204",
                IDRANGOTBL null
              }
            ]
          }
        ]
      }
    ]
  }
}
```

#### 3. Garantias opcionales con tramos

Se da en aquellos seguros que ademas de tener incluidas las condiciones generales de la póliza, dan la opción de añadir garantias o coberturas a dicha póliza pagando una prima extra.

Si nos fijamos en RESULT->CONTENT[0]->GARANTIAS[1]->INCLUIDA nos indica que su valor es `2`, esto quiere decir que es una garantia opcional no incluida en las condiciones generales de la póliza.

Para calcular el precio final tendriamos que sumar:

Precio Final = `RESULT->CONTENT[0]->PRIMATOTAL` + `RESULT->CONTENT[0]->GARANTIAS[1]->SUMASASEGURADASPREDEFINIDAS[1]->PRIMA`

Precio Final = `12` + `2` = `14 €`

**Datos de la solicitud (2)**
```json
{
  "url": "https://api.intermundial.com/v1/es/rate/601/insurances.json?values=[1461,1463]",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI3MzI0NzYsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InJvZWFqcnVncm91YjQ4aWQ4a",
    "cache-control": "no-cache",
  }
}
```


****
**Respuesta con éxito (2)**

```json
{
  HTTP_STATUS 200,
  CODE "OK",
  MESSAGE "OK",
  RESPONSE_FORMAT "JSON",
  RESULT {
    CONTENT [
      {
        IDTARIFA "601",
        IDTARIFAPER "764",
        PRIMATOTAL 12,
        COEFICIENTEIMPUESTOS "0.0615",
        GARANTIAS [
          {
            ID "200000021597",
            IDGARANTIA "10219",
            IDGARANTIATARIFA "21597",
            IDGARANTIARAMO null,
            NOMBREGARANTIA "Gastos Médicos",
            INCLUIDA "1",
            DESINCLUIDA "Si",
            SUMAASEGURADATIPO "2",
            DESSUMAASEGURADATIPO "Lista",
            SUMAASEGURADAMIN null,
            SUMAASEGURADAMAX null,
            TBLGARANTIA null,
            SUMASASEGURADASPREDEFINIDAS [
              {
                SELPORDEFECTO "1",
                SUMAASEGURADA "50000",
                PRIMA "0",
                IDTARIFALST "1208",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "100000",
                PRIMA "25",
                IDTARIFALST "1214",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "150000",
                PRIMA "50",
                IDTARIFALST "1209",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "200000",
                PRIMA "75",
                IDTARIFALST "1210",
                IDRANGOTBL null
              }
            ]
          },
          {
            ID "200000021641",
            IDGARANTIA "10381",
            IDGARANTIATARIFA "21641",
            IDGARANTIARAMO null,
            NOMBREGARANTIA "Deportes",
            INCLUIDA "2",
            DESINCLUIDA "Opcional",
            SUMAASEGURADATIPO "2",
            DESSUMAASEGURADATIPO "Lista",
            SUMAASEGURADAMIN null,
            SUMAASEGURADAMAX null,
            TBLGARANTIA null,
            SUMASASEGURADASPREDEFINIDAS [
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "1000",
                PRIMA "1",
                IDTARIFALST "1202",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "2000",
                PRIMA "2",
                IDTARIFALST "1203",
                IDRANGOTBL null
              },
              {
                SELPORDEFECTO "0",
                SUMAASEGURADA "3000",
                PRIMA "3",
                IDTARIFALST "1204",
                IDRANGOTBL null
              }
            ]
          }
        ]
      }
    ]
  }
}
```