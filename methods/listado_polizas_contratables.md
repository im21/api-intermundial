## Listado de pólizas contratables

Llamada que devuelve un objeto con información sobre las pólizas que puede contratar el usuario actual 

```
POST /v1/:_locale/users.:_format/availablepolicies
```

****
**Parámetros de la llamada**

| Name | Type | Description |
| --- | --- | --- |
| _locale | string | Código de lenguaje según el standard [ISO 3166-1 alfa-2](http://www.iso.org/iso/home/standards/country_codes.htm) |
| _format | string | Formato del salida del documento [json, xml] |

****
**Datos de la Solicitud**

```
{
  "url": "https://secure-aleon.intermundial.com/api/v1/es/users.json/availablepolicies",
  "method": "GET",
  "headers": {
    "cache-control": "no-cache"    
  }
}
```


****
**Respuesta con éxito**

De entre todas las propiedades que contiene el objeto devuelto, destacamos:

* RESULT->CONTENT[0]->NUMPOLIZA => Número de la póliza a contratar.
* RESULT->CONTENT[0]->LEYENDACOMERCIAL => El nombre comercial del producto.

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: [
      {
        IDPOLIZA: "15800",
        NUMPOLIZA: "MMC-PRUEBA8",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "775",
        TARIFADESC: "PRUEBA PRIMAFIJA",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4781",
        PRODUCTOCIA: "PRUEBA PRIMA FIJA",
        LEYENDALISTADOS: "PRUEBA PRIMA FIJA",
        LEYENDACOMERCIAL: "PRUEBA PRIMA FIJA",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789049",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789048",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15799",
        NUMPOLIZA: "MMC-PRUEBA7",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "774",
        TARIFADESC: "PRUEBA DURACION/EDAD",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4780",
        PRODUCTOCIA: "PRUEBA DURACION/EDAD",
        LEYENDALISTADOS: "PRUEBA DURACION/EDAD",
        LEYENDACOMERCIAL: "PRUEBA DURACION/EDAD",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789047",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789046",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15465",
        NUMPOLIZA: "MMC-PRUEBA6",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "773",
        TARIFADESC: "PRUEBA DURACION/IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4779",
        PRODUCTOCIA: "PRUEBA DURACION/IMPORTE",
        LEYENDALISTADOS: "PRUEBA DURACION/IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA DURACION/IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789045",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789043",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15464",
        NUMPOLIZA: "MMC-PRUEBA5",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "772",
        TARIFADESC: "PRUEBA DESTINO/IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4704",
        PRODUCTOCIA: "PRUEBA DESTINO/IMPORTE",
        LEYENDALISTADOS: "PRUEBA DESTINO/IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA DESTINO/IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789042",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789041",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15463",
        NUMPOLIZA: "MMC-PRUEBA4",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "771",
        TARIFADESC: "PRUEBA IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4703",
        PRODUCTOCIA: "PRUEBA IMPORTE",
        LEYENDALISTADOS: "PRUEBA IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789040",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789039",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15348",
        NUMPOLIZA: "MMC-PRUEBA3",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "622",
        TARIFADESC: "PRUEBA PORCENTAJE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "512",
        RAMO: "ANULACION MINORISTA",
        RAMOCLIENTE: "ANULACION MINORISTA",
        IDPRODUCTOCIA: "4683",
        PRODUCTOCIA: "PRUEBA PORCENTAJE",
        LEYENDALISTADOS: "PRUEBA PORCENTAJE",
        LEYENDACOMERCIAL: "PRUEBA PORCENTAJE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789038",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789037",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15347",
        NUMPOLIZA: "MMC-PRUEBA2",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "621",
        TARIFADESC: "PRUEBA CANCELACION",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "512",
        RAMO: "ANULACION MINORISTA",
        RAMOCLIENTE: "ANULACION MINORISTA",
        IDPRODUCTOCIA: "4684",
        PRODUCTOCIA: "PRUEBA CANCELACION",
        LEYENDALISTADOS: "PRUEBA CANCELACION",
        LEYENDACOMERCIAL: "PRUEBA CANCELACION",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789035",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789033",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15319",
        NUMPOLIZA: "MMC-PRUEBA",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "601",
        TARIFADESC: "PRUEBA",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4677",
        PRODUCTOCIA: "PRUEBA",
        LEYENDALISTADOS: "PRUEBA",
        LEYENDACOMERCIAL: "PRUEBA",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789031",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789029",
            NOMBRE: "COND_GEN_SEG_TOTALSPORTS_MMC-970033.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      }
    ]
  }
}
```