# API Intermundial

### Métodos

**Sesión**
* [Login](methods/login.md)
* [Refrescar token de autenticación](methods/actualizar_token_sesion.md)
* [Logout](methods/logout.md)
* [Información de la sesión](methods/informacion_de_la_sesion.md)



**Pólizas**
* [Listado de pólizas contratables](methods/listado_polizas_contratables.md)
* [Recuperar documentos de una póliza](methods/recuperar_documentos.md)
* [Opciones de una póliza](methods/opciones_de_una_poliza.md)
* [Tarificar una póliza](methods/tarificar_una_poliza.md)



**Seguros**
* [Contratar un seguro](methods/alta_seguro.md)
* [Anular un seguro](methods/baja_seguro.md)


****
### Ejemplos
* [Tarificar una póliza](examples/tarificar_una_poliza.md)
* [Contratar un seguro](examples/contratacion.md)
* [Recuperar un certificado](examples/recuperar_certificado.md)
* [Recuperar documentos de una póliza](examples/recuperar_documentos.md)

