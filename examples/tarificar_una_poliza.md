## Opciones de una póliza

**Datos de la solicitud**

```json
{
  "url": "https://api.intermundial.com/v1/es/rate/insurances.json/MMC-PRUEBA",
  "method": "GET",
  "headers": {
  "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NjEyNTQwODcsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjdyNTdtZmRoOTUzb2JqNjRuMWk1NWQzMWo0IiwidXNlcklkIjoyfQ.2OMFZprx5YaAlUYYmTxvjhXOTS7R56dak8cWcg8FxNk",
      "cache-control": "no-cache"    
  }
}
```

****
**Respuesta con éxito**

*Para este ejemplo seleccionaremos DESTINO ```Europa``` y DURACION ```De 1 a 34 días```.*
> Es importante tener en cuenta el campo ```NUMPARAM``` que establece el orden de los parámetros al tarificar una póliza.
>
> ```---->``` son valores que necesitaremos para posteriores solicitudes.

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: [
      {
  --->  IDTARIFA: "601",
        DESCRIPCION: "PRUEBA",
        TARIFAPLANA: "0",
        IDDIVISA: "0",
        INFOCOMERCIAL: {
  --->    INFOCOMERCIAL1: "25.6",
  --->    INFOCOMERCIAL2: null
        },
        PARAMS: [
          {
  --->      IDPARAM: "605",
            NUMPARAM: "1",
            DESCRIPCION: "Destino",
            TIPODATO: "S",
            DESTIPODATO: null,
            TBLAUX: null,
  --->      VALORES: [
              {
                IDVAL: "1462",
                DESCRIPCION: "Mundo",
                NDESDE: "0",
                NHASTA: "0"
              },
              {
                IDVAL: "1461",
                DESCRIPCION: "Europa",
                NDESDE: "0",
                NHASTA: "0"
              }
            ]
          },
          {
  --->      IDPARAM: "606",
            NUMPARAM: "2",
            DESCRIPCION: "Duración",
            TIPODATO: "R",
            DESTIPODATO: "días",
            TBLAUX: null,
  --->      VALORES: [
              {
                IDVAL: "1463",
                DESCRIPCION: "De 1 a 34 días",
                NDESDE: "1",
                NHASTA: "34"
              },
              {
                IDVAL: "1464",
                DESCRIPCION: "De 35 a 68 días",
                NDESDE: "35",
                NHASTA: "68"
              }
            ]
          }
        ],
        POLIZAS: [
          {
            IDPOLIZA: "15319",
            NUMPOLIZA: "MMC-PRUEBA",
            RIESGODESC: "PRUEBA",
            IDDIVISA: "0"
          }
        ],
  --->  TIPOTARIFA: "DURACION"
      }
    ]
  }
}
```

---


## Tarificar una póliza

**Datos de la solicitud**

```json
{ 
  "url": "https://api.intermundial.com/v1/es/rate/601/insurances.json?values=[1461,1463]",
  "method": "GET",
  "headers": {
    "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI3MzI0NzYsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InJvZWFqcnVncm91YjQ4aWQ4a",
    "cache-control": "no-cache",    
  }
}
```


****
**Respuesta con éxito**

```json 
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": [
      {
        "IDTARIFA": "601",
        "IDTARIFAPER": "563",
        "PRIMATOTAL": 12.75,
        "COEFICIENTEIMPUESTOS": "0.0615",
        "GARANTIAS": [
          {
            "ID": "200000007563",
            "IDGARANTIA": "10336",
            "IDGARANTIATARIFA": "7563",
            "IDGARANTIARAMO": null,
            "NOMBREGARANTIA": "Heliesqui",
            "INCLUIDA": "2",
            "DESINCLUIDA": "Opcional",
            "SUMAASEGURADATIPO": "0",
            "DESSUMAASEGURADATIPO": "Fija",
            "SUMAASEGURADAMIN": null,
            "SUMAASEGURADAMAX": null,
            "TBLGARANTIA": null,
            "SUMASASEGURADASPREDEFINIDAS": [
              {
                "SELPORDEFECTO": "1",
                "SUMAASEGURADA": "3000",
                "PRIMA": "10",
                "IDTARIFALST": null,
                "IDRANGOTBL": null
              }
            ]
          }
        ]
      }
    ]
  }
}
```
---