### Ejemplo de Contratación

A continuación, vamos a describir el proceso completo para contratar un seguro con los siguientes parámetros:

* DESTINO:  **Mundo**
* DURACIÓN: **44 días**
* COBERTURA ADICIONAL: **Heliesquí**

Los pasos a seguir serán los ya mencionados:

* 1.- [Inicio de sesión](../methods/login.md)
* 2.- [Obtener opciones de tarificación de la póliza](../methods/opciones_de_una_poliza.md)
* 3.- [Consultar tarifa y opciones de ampliación de garantías](../methods/tarificar_una_poliza.md) \(si procede\)
* 4.- Construir XML
* 5.- [Contratar el seguro](../methods/alta_seguro.md)

---

### 1.- Inicio de Sesión

Realizamos la llamada de inicio de sesión.Si el resultado es correcto nos devolverá un objeto con información sobre el usuario y la sesión que se inicia, destacando las siguientes:

* RESULT-&gt;CONTENT-&gt;ID =&gt; Código de identificación del usuario.
* RESULT-&gt;CONTENT-&gt;SESSIONKEY =&gt; Token que necesitaremos en las posteriores llamadas como cabecera "AuthToken".

---

**Datos de la solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/auth.json",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "user": "sandbox",
    "pass": "sandbox_intermundial",
  }
}
```

---

**Respuesta con éxito**

```json
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": {
--->  "ID": "16",
      "USER": "sandbox",      
      "LASTLOGINDATE": "31/08/2016 16:11:00",
      "MODIFIEDDATE": null,
      "CREATEDDATE": null,
      "STATUS": 1,
      "DELETED": 0,
      "ROLES": "1,3,4,5",
      "ACCESSIBLERESOURCES": [
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::postAuthAction",
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::deleteAuthAction",
      ],
      "SESSIONKEYVALIDTIME": 1472654460,
--->  "SESSIONKEY": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
      "ENVIROMENT": "prod",
      "USERTYPE": "partner",
      "IDCLIENTE": "null"
    }
  }
}
```

---

### 2.- Obtener opciones de tarificación de la póliza

Con el número de póliza y token obtenido en la llamada anterior, realizamos una llamada que nos devolverá un objeto con las posibilidades de contratación de esta póliza y el tipo de tarificador que marcará la estructura del XML a construir.

Para nuestro ejemplo, la cobertura deseada es:

* Ámbito: **Mundo** \(IDVAL = 1462\)
* Duración: **44 días** \(IDVAL = 1464\)

A tener en cuenta los siguientes valores de la respuesta:

* IDTARIFA: Identificador de la tarifa asociada a esta póliza.
* NUMPARAM: Define la posición de los parámetros para la siguiente solicitud.
* TIPOTARIFA: Define el tipo de tarificación de la póliza

---

**Datos de la solicitud**

```json
{  
  --->   "url": "https://api.intermundial.com/v1/es/rate/insurances.json/MMC-PRUEBA2",
         "method": "GET",
         "headers": {
  --->       "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
             "cache-control": "no-cache",    
             "content-type": "application/x-www-form-urlencoded"
         }
}
```

---

**Respuesta con éxito**

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: [
      {
  --->  IDTARIFA: "601",
        DESCRIPCION: "PRUEBA",
        PARAMS: [
          {
            IDPARAM: "605",
  --->      NUMPARAM: "1", 
            DESCRIPCION: "Destino",
            TIPODATO: "S",
            DESTIPODATO: null,
            TBLAUX: null,
            VALORES: [
              {
  --->          IDVAL: "1462",
                DESCRIPCION: "Mundo",
                NDESDE: "0",
                NHASTA: "0"
              },
              {
                IDVAL: "1461",
                DESCRIPCION: "Europa",
                NDESDE: "0",
                NHASTA: "0"
              }
            ]
          },
          {
            IDPARAM: "606",
  --->      NUMPARAM: "2",
            DESCRIPCION: "Duración",
            TIPODATO: "R",
            DESTIPODATO: "días",
            TBLAUX: null,
            VALORES: [
              {
                IDVAL: "1463",
                DESCRIPCION: "De 1 a 34 días",
                NDESDE: "1",
                NHASTA: "34"
              },
              {
  --->          IDVAL: "1464",
                DESCRIPCION: "De 35 a 68 días", 
                NDESDE: "35",
                NHASTA: "68"
              }
            ]
          }
        ],
        POLIZAS: [
          {
            IDPOLIZA: "15319",
            NUMPOLIZA: "MMC-PRUEBA",
            RIESGODESC: "PRUEBA"
          }
        ],
  --->  TIPOTARIFA: "DURACION"
      }
    ]
  }
}
```

---

### 3.- Consultar tarifa y opciones de ampliación de garantías \(coberturas adicionales\)

Solamente en el caso en el que queramos contratar garantías adicionales, con los valores IDTARIFA e IDVAL obtenidos en la llamada anterior, realizamos una nueva solicitud para recibir un objeto tarifa que, además de contener el precio total del seguro, puede contener un array de GARANTIAS, en este caso, con las siguientes opciones:

* Ampliación de la cobertura de los servicios contratados.
  Por ejemplo, se podrían aumentar los gastos médicos cubiertos por la póliza de **100.000 €** a **150.000 €**.
* Incorporación de servicios o coberturas a una póliza que en su modo base no vienen incluidas. 
  Por ejemplo, añadir la cobertura opcional **Heliesqui** a nuestro seguro.

Destacamos los siguientes valores:

* SELPORDEFECTO: Si este valor es 1 implica que esta es la opción por defecto para la cobertura Heliesqui
* SUMAASEGURADA: La suma total asegurada por esta cobertura.
* PRIMA: Prima adicional que se va a cobrar por la contratación de esta cobertura.
* ID \(array garantias\): Identificador único de la garantia.
* INCLUIDA: Si este valor es 1, la cobertura estará incluida en el precio inicial de la póliza; si es 2, no vendrá incluida y se podrá contratar si así se desea.

---

**Datos de la solicitud**

```json
{  
     "url": "https://api.intermundial.com/v1/es/rate/601/insurances.json?values=[1462,1464]",
     "method": "GET",
     "headers": {
  --->   "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
         "cache-control": "no-cache",    
         "content-type": "application/x-www-form-urlencoded"
     }
}
```

---

**Respuesta con éxito**

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: [
      {
        IDTARIFA: "601",
        IDTARIFAPER: "563",
        PRIMATOTAL: 36.48,
        COEFICIENTEIMPUESTOS: "0.0615",
        GARANTIAS: [
          {
  --->      ID: "200000007563",
            IDGARANTIA: "10336",
            IDGARANTIATARIFA: "7563",
            IDGARANTIARAMO: null,
            NOMBREGARANTIA: "Heliesqui",
  --->      INCLUIDA: "2",
            DESINCLUIDA: "Opcional",
            SUMAASEGURADATIPO: "0",
            DESSUMAASEGURADATIPO: "Fija",
            SUMAASEGURADAMIN: null,
            SUMAASEGURADAMAX: null,
            TBLGARANTIA: null,
            SUMASASEGURADASPREDEFINIDAS: [
              {
  --->          SELPORDEFECTO: "1",
  --->          SUMAASEGURADA: "3000",
  --->          PRIMA: "10",
  --->          IDTARIFALST: null,
  --->          IDRANGOTBL: null
              }
            ]
          }
        ]
      }
    ]
  }
}
```

---

### 4.- Construir XML

Una vez tenemos toda la información necesaria, ya podemos construir un XML válido con dicha información estructurada dependiendo del tipo de seguro a contratar y las coberturas adicionales si las hubiere.

Dependiendo del tipo de tarificador, la estructura varia para incluir la información apropiada.

Para un pasajero, la información del asegurado será mostrada en la etiqueta `<OBJ_ASEGURADO>`.

Para más de un pasajero, la información del asegurado principal será mostrada en la etiqueta `<OBJ_ASEGURADO>` y aparece la etiqueta `<ARR_PASAJEROS>` para \(PAX - 1\) etiquetas `<OBJ_PASAJERO>`, donde se incluirá la información disponible del resto de asegurados.

Como datos básicos de la contratación vamos a necesitar:

* NÚMERO DE PÓLIZA.
* ACCION: ALTA, BAJA o MODIFICACIÓN.
* LOCALIZADOR: Referencia del cliente a los datos de su reserva.
* FECHA EFECTO: Fecha inicio de la cobertura.
* FECHA BAJA: Fecha fin de la cobertura.
* PAX: Número de pasajeros.
* NOMBRE Y APELLIDOS del ASEGURADO PRINCIPAL y del resto de pasajeros \(si hubiere\).

De las llamadas anteriores, necesitamos:

* Del objeto Sesión
  * Token de sesión \(RESULT-&gt;CONTENT-&gt;SESSIONKEY para la llamada\)

* Del objeto Tarificador
  * Descripción de los parámetros, escrito tal y como nos lo devuelve la llamada, respetando mayúsculas, tildes, etc... \(RESULT-&gt;CONTENT\[0\]-&gt;PARAMS\[n\]-&gt;DESCRIPCION para la etiqueta `<STR_DESTINO>`\)
  * Tipo de tarifa \(RESULT-&gt;CONTENT\[0\]-&gt;TIPOTARIFA para la etiqueta `<STR_DURACION_IMPORTE>`\)

* Del objeto Tarifa \(si se contratan garantías adicionales\)
  * ID \(array garantias\): Identificador único de la garantia \(RESULT-&gt;CONTENT\[0\]-&gt;GARANTIAS\[n\]-&gt;ID\).
  * Suma total asegurada de las coberturas \(RESULT-&gt;CONTENT\[0\]-&gt;GARANTIAS\[n\]-&gt;SUMASASEGURADASPREDEFINIDAS\[0\]-&gt;SUMAASEGURADA\).
  * PRIMA: Prima adicional que se va a cobrar por la contratación de esta cobertura \(RESULT-&gt;CONTENT\[0\]-&gt;GARANTIAS\[n\]-&gt;SUMASASEGURADASPREDEFINIDAS\[0\]-&gt;PRIMA\).

---

Esta es la estructura del XML a enviar en la llamada de contratación:

```xml
<OBJ_COMUNICACION>
      <ARR_SEGUROS>
          <OBJ_SEGURO>
              <STR_ACCION>ALTA</STR_ACCION>
              <STR_NOMBRE_SEGURO></STR_NOMBRE_SEGURO>
              <STR_NPOLIZA>MMC-PRUEBA</STR_NPOLIZA>
              <STR_LOCALIZADOR>interm-4</STR_LOCALIZADOR>
```

Tarificador Tipo DESTINO_DURACION
```xml
              <STR_DESTINO>Mundo</STR_DESTINO>
              <STR_DURACION_IMPORTE>DESTINO_DURACION</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>         
```

Tarificador Tipo MODALIDAD_IMPORTE
```xml
              <STR_DURACION_IMPORTE>MODALIDAD_IMPORTE</STR_DURACION_IMPORTE>
              <STR_MODALIDAD>Viaje Combinado</STR_MODALIDAD>            
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>            
              <DEC_IMPORTE_RESERVA>5000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo PORCENTAJE
```xml
              <STR_DURACION_IMPORTE>PORCENTAJE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo IMPORTE
```xml
              <STR_DURACION_IMPORTE>IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>300</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo DESTINO_IMPORTE
```xml
              <STR_DESTINO>Mundo</STR_DESTINO>
              <STR_DURACION_IMPORTE>DESTINO_IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo DURACION_IMPORTE
```xml
              <STR_DURACION_IMPORTE>DURACION_IMPORTE</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
```

Tarificador Tipo EDAD_DURACION
```xml
              <STR_DURACION_IMPORTE>EDAD_DURACION</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
              <DEC_IMPORTE_RESERVA>1000</DEC_IMPORTE_RESERVA>
              ...
              <!-- Es importante que en este tipo de tarifa se indique la fecha de nacimiento del asegurado -->
              <OBJ_ASEGURADO>
                    <STR_NOMBRE_APELLIDOS>Fernandez Pérez, Roberto</STR_NOMBRE_APELLIDOS>
                    <STR_NIF></STR_NIF>
                    <STR_PASAPORTE></STR_PASAPORTE>
                    <DATE_FECHA_NACIMIENTO>1984-05-15</DATE_FECHA_NACIMIENTO>
                    ...
              <OBJ_ASEGURADO>
              ...
```

Tarificador Tipo PRIMAFIJA
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

En el caso de que queramamos validar que el importe que hemos calculado es el correcto tendremos que proporcionar despues de la etiqueta `<INT_PAX>` la etiqueta `<DEC_IMPORTE_A_VALIDAR>`
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DEC_IMPORTE_A_VALIDAR>12.75</DEC_IMPORTE_A_VALIDAR>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

Tambien, de manera opcional y si se quisiera especificar el pais de origen y de destino se puede hacer con las etiquetas `<STR_PAIS_ORIGEN>` y `<STR_PAIS_DESTINO>`
```xml
              <STR_DURACION_IMPORTE>PRIMAFIJA</STR_DURACION_IMPORTE>
              <INT_PAX>1</INT_PAX>
              <DEC_IMPORTE_A_VALIDAR>12.75</DEC_IMPORTE_A_VALIDAR>
              <STR_PAIS_ORIGEN>España</STR_PAIS_ORIGEN>
              <STR_PAIS_DESTINO>Italia</STR_PAIS_DESTINO>
              <DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
              <DATE_FECHA_BAJA>2016-09-25</DATE_FECHA_BAJA>
```

El resto de la estructura es común para todos los tipos de tarificador.

```xml
            <OBJ_GARANTIAS>
                <GARANTIA>
                    <ID>200000007563</ID>
                    <SUMA_ASEGURADA>3000</SUMA_ASEGURADA>
                    <PRIMA>10</PRIMA>
                    <ID_TARIFA_LIST></ID_TARIFA_LIST>
                    <ID_RANGO_TABLA></ID_RANGO_TABLA>
                </GARANTIA>
            </OBJ_GARANTIAS>

            <OBJ_ASEGURADO>
                <STR_NOMBRE_APELLIDOS>Fernandez Pérez, Roberto</STR_NOMBRE_APELLIDOS>
                <STR_NIF></STR_NIF>
                <STR_PASAPORTE></STR_PASAPORTE>
                <DATE_FECHA_NACIMIENTO></DATE_FECHA_NACIMIENTO>
                <STR_SEXO></STR_SEXO>
                <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                <STR_BARRIO></STR_BARRIO>
                <STR_LOCALIDAD></STR_LOCALIDAD>
                <STR_PROVINCIA></STR_PROVINCIA>
                <STR_CP></STR_CP>
                <STR_PAIS></STR_PAIS>
                <STR_EMAIL></STR_EMAIL>
                <STR_TELEFONO></STR_TELEFONO>
                <STR_EXTENSION></STR_EXTENSION>
            </OBJ_ASEGURADO>

            <ARR_PASAJEROS>             
                <OBJ_PASAJERO>
                    <STR_NOMBRE_APELLIDOS>De las Torres, Luis</STR_NOMBRE_APELLIDOS>
                    <STR_NIF></STR_NIF>
                    <STR_PASAPORTE></STR_PASAPORTE>
                    <DATE_FECHA_NACIMIENTO></DATE_FECHA_NACIMIENTO>
                    <STR_SEXO></STR_SEXO>
                    <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                    <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                    <STR_BARRIO></STR_BARRIO>
                    <STR_LOCALIDAD></STR_LOCALIDAD>
                    <STR_PROVINCIA></STR_PROVINCIA>
                    <STR_CP></STR_CP>
                    <STR_EMAIL></STR_EMAIL>
                    <STR_TELEFONO></STR_TELEFONO>
                </OBJ_PASAJERO>                
                <OBJ_PASAJERO>
                    <STR_NOMBRE_APELLIDOS>De las Torres, Roberto</STR_NOMBRE_APELLIDOS>
                    <STR_NIF></STR_NIF>
                    <STR_PASAPORTE></STR_PASAPORTE>
                    <DATE_FECHA_NACIMIENTO></DATE_FECHA_NACIMIENTO>
                    <STR_SEXO></STR_SEXO>
                    <STR_DIRECCION_CALLE></STR_DIRECCION_CALLE>
                    <STR_DIRECCION_NUMERO></STR_DIRECCION_NUMERO>
                    <STR_BARRIO></STR_BARRIO>
                    <STR_LOCALIDAD></STR_LOCALIDAD>
                    <STR_PROVINCIA></STR_PROVINCIA>
                    <STR_CP></STR_CP>
                    <STR_EMAIL></STR_EMAIL>
                    <STR_TELEFONO></STR_TELEFONO>
                </OBJ_PASAJERO>
           </ARR_PASAJEROS> 

        </OBJ_SEGURO>
    </ARR_SEGUROS>
</OBJ_COMUNICACION>
```

Para contratar seguros anuales, habrá que incluir los parámetros necesarios (DESTINO, DURACIÓN, MODALIDAD, ...), a los que se añadirá la fecha de inicio que se elija, siendo la fecha de fin el día anterior pero del año siguiente.

Ejemplo para TOTAL TRAVEL ANNUAL:

```xml
<STR_MODALIDAD>Individual</STR_MODALIDAD>
<STR_DESTINO>Mundo</STR_DESTINO>
<STR_DURACION_IMPORTE>MODALIDAD_DESTINO</STR_DURACION_IMPORTE>
<INT_PAX>2</INT_PAX>
<DATE_FECHA_EFECTO>2016-09-20</DATE_FECHA_EFECTO>
<DATE_FECHA_BAJA>2017-09-19</DATE_FECHA_BAJA>
```



---

### 5.- Contratar seguro

Con el XML construido, ya sólo resta lanzar la llamada de contratación.

---

**Datos de la solicitud**

```json
{  
       "url": "https://api.intermundial.com/v1/es/partners/load/bulk.json",
       "method": "POST",
       "headers": {
  ---> "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
       "cache-control": "no-cache",    
       "content-type": "application/x-www-form-urlencoded"
  }
  data: {
      xml: "<OBJ_COMUNICACION>...</OBJ_COMUNICACION>"
  }
}
```

---

**Respuesta con éxito**

El valor IDVIAJE \(Id Poliza + Id Asegurado\) es un ID imprescindible si más adelante se quiere modificar o eliminar un seguro o [recuperar un certificado](recuperar_certificado.md).

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: {
      SEGURO0: {
        HTTP_STATUS: 200,
        CODE: "OK",
        MESSAGE: "OK",
        RESPONSE_FORMAT: "JSON",
        RESULT: {
          CONTENT: {
  --->      IDVIAJE: "00000153190000000019",
            IDPOLIZA: "15319",
            NUMPOLIZA: "MMC-PRUEBA",
            LOCALIZADOR: "intm-prueba-1",
            FECHAINICIOVIAJE: "20/09/2016",
            FECHAFINVIAJE: "25/09/2016",
            COSTEVIAJE: null,
            NPAX: "3",
            AGENCIANIF: "B81577231",
            AGENCIAID: null,
            CERTIFICADO2: null,
            PRIMATOTAL: "84.72",
            IDTARIFA: "601",
            PARAMS: [
              {
                IDPARAM: "605",
                NUMPARAM: "1",
                VALOR: {
                  IDVAL: "1462",
                  DESCRIPCION: "Mundo"
                }
              },
              {
                IDPARAM: "606",
                NUMPARAM: "2",
                VALOR: {
                  IDVAL: "1463",
                  DESCRIPCION: "De 1 a 34 días"
                }
              }
            ],
            ASEGURADOS: [
              {
                IDASEGURADO: "19",
                IDASEGURADODEP: null,
                IDCLIENTE: "17102575",
                NOMBRE: "Fernandez Pérez, Roberto",
                TIPODOC: "NIF",
                NDOCUMENTO: null,
                DOMICILIO: null,
                CPO: null,
                LOCALIDAD: null,
                PROVINCIA: null,
                TELMOVIL: null,
                EMAIL: null,
                IDBILLETE: "000001531900000000190017102575"
              },
              {
                IDASEGURADO: null,
                IDASEGURADODEP: "185",
                IDCLIENTE: "17102576",
                NOMBRE: "De las Torres, Luis",
                TIPODOC: "NIF",
                NDOCUMENTO: null,
                DOMICILIO: null,
                CPO: null,
                LOCALIDAD: null,
                PROVINCIA: null,
                TELMOVIL: null,
                EMAIL: null,
                IDBILLETE: "000001531900000000000017102576"
              },
              {
                IDASEGURADO: null,
                IDASEGURADODEP: "186",
                IDCLIENTE: "17102577",
                NOMBRE: "De las Torres, Roberto",
                TIPODOC: "NIF",
                NDOCUMENTO: null,
                DOMICILIO: null,
                CPO: null,
                LOCALIDAD: null,
                PROVINCIA: null,
                TELMOVIL: null,
                EMAIL: null,
                IDBILLETE: "000001531900000000000017102577"
              }
            ],
            RECIBOS: [
              {
                IDRECIBO: "1600922083",
                NUMRECIBO: null,
                FECHAEFECTO: "20/09/2016",
                FECHAVENCIMIENTO: "25/09/2016",
                TIPO: "Suplemento",
                SECUENCIA: "1/1",
                RIESGODESC: "Fernandez Pérez, Roberto",
                ESTADO: "0",
                ESTADODESC: "GESTION COBRO",
                REVISADO: "1",
                REMESADO: "0",
                IMPORTETOTAL: "84.72"
              }
            ],
            CERTIFICADO: {
              ORIGINALMIME: "application/x-empty",
              ENCODEDMIME: null,
              MD5HASH: null,
              FILE: null
            }
          }
        }
      }
    }
  }
}
```

