### Ejemplo de Recuperación de un Certificado

A continuación, vamos a describir el proceso completo para recuperar el certificado de un asegurado con los siguientes parámetros:

* NÚMERO DE PÓLIZA
* LOCALIZADOR 

Los pasos a seguir serán:
* 1.- [Inicio de sesión](../methods/login.md)
* 2.- Recuperar Datos de la Reserva
* 3.- Recuperar Certificado


****
### 1.- Inicio de Sesión
Realizamos la llamada de inicio de sesión. Si el resultado es correcto nos devolverá un objeto con información sobre el usuario y la sesión que se inicia, destacando las siguientes:

* RESULT->CONTENT->ID => Código de identificación del usuario.
* RESULT->CONTENT->SESSIONKEY => Token que necesitaremos en las posteriores llamadas como cabecera "AuthToken".


****
**Datos de la solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/auth.json",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "user": "sandbox",
    "pass": "sandbox_intermundial",
  }
}
```

****
**Respuesta con éxito**

```json
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": {
--->  "ID": "16",
      "USER": "sandbox",      
      "LASTLOGINDATE": "31/08/2016 16:11:00",
      "MODIFIEDDATE": null,
      "CREATEDDATE": null,
      "STATUS": 1,
      "DELETED": 0,
      "ROLES": "1,3,4,5",
      "ACCESSIBLERESOURCES": [
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::postAuthAction",
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::deleteAuthAction",
      ],
      "SESSIONKEYVALIDTIME": 1472654460,
--->  "SESSIONKEY": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
      "ENVIROMENT": "prod",
      "USERTYPE": "partner",
      "IDCLIENTE": "null"
    }
  }
}
```


****
### 2.- Recuperar Datos de la Reserva

Una vez con la sesión iniciada, realizamos otra llamada para recuperar los datos de la reserva con el NÚMERO DE PÓLIZA y el LOCALIZADOR.


****
**Datos de la solicitud**

```json
 {
    "url": "https://api.intermundial.com/v1/es/insurances.json/attributes/MMC-PRUEBA/prueba_intermundial_123456/reservationdata",
     "method": "GET",
     "headers": {
         "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzY4MDg5OTIsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjFyZWZtczA4cXNtc29yZzV2Zm82c2FqZDUwIiwidXNlcklkIjoiMjA5ODgifQ.dTV5kMy1Qlwh4QnXLQ__VKryWkcWkrjZR8geWZ79MBg",
         "cache-control": "no-cache",
         "content-type": "application/x-www-form-urlencoded"
     }}
```


****
**Respuesta con éxito**

De entre todos los datos que incluye el objeto datos de la reserva, necesitamos el IDVIAJE correspondiente al certificado que queremos recuperar.

```json
{
    "HTTP_STATUS": 200,
    "CODE": "OK",
    "MESSAGE": "OK",
    "RESPONSE_FORMAT": "JSON",
    "RESULT": {
        "CONTENT": {
   --->     "IDVIAJE": "00000153190000000002",
            "IDPOLIZA": "15319",
            "NUMPOLIZA": "MMC-PRUEBA",
            "LOCALIZADOR": "prueba_intermundial_123456",
            "FECHAINICIOVIAJE": "20/09/2016",
            "FECHAFINVIAJE": "25/09/2016",
            "COSTEVIAJE": null,
            "NPAX": "1",            "AGENCIANIF": "B81577231",
            "AGENCIAID": null, 
            "CERTIFICADO2": null,
            "PRIMATOTAL": "12.75",            "IDTARIFA": "601",
            "PARAMS": [
               { 
                 "IDPARAM": "605",
                 "NUMPARAM": "1",
                 "VALOR": {
                    "IDVAL": "1461",
                    "DESCRIPCION": "Europa"
                 }
               },
               {
                  "IDPARAM": "606",
                  "NUMPARAM": "2",
                  "VALOR": {
                      "IDVAL": "1463",
                      "DESCRIPCION": "De 1 a 34 días"
                  }
                }
            ],
            "ASEGURADOS": [
               {
                  "IDASEGURADO": "2",
                  "IDASEGURADODEP": null,
                  "IDCLIENTE": "16869873",
                  "NOMBRE": "Fernandez Pérez, Roberto",
                  "TIPODOC": "NIF",
                  "NDOCUMENTO": null,
                  "DOMICILIO": null,
                  "CPO": null,  
                  "LOCALIDAD": null,
                  "PROVINCIA": null,
                  "TELMOVIL": null,
                  "EMAIL": null
               }
            ], 
            "RECIBOS": [
                {
                  "IDRECIBO": "1600821325",
                  "NUMRECIBO": null,
                  "FECHAEFECTO": "20/09/2016",
                  "FECHAVENCIMIENTO": "25/09/2016",
                  "TIPO": "Suplemento",
                  "SECUENCIA": "1/1", 
                  "RIESGODESC": "Fernandez Pérez, Roberto",
                  "ESTADO": "0",
                  "ESTADODESC": "GESTION COBRO", 
                  "REVISADO": "1",
                  "REMESADO": "0",
                  "IMPORTETOTAL": "12.75"
                }
            ]
        }
    }
}
```


****
### 3.- Recuperar Certificado

Una vez tenemos el ID del Viaje, lanzamos la última llamada para descargarnos el certificado.


****
**Datos de la solicitud**

```json
{   "url": "https://api.intermundial.com/v1/es/insurances.json/travelid/00000100900000000755/certificate",
    "method": "GET",
    "headers": {
        "authtoken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzY4MDMyNDIsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6IjJsZXZzbmJyaDI4azgyamplZGpnazRyZ2YyIiwidXNlcklkIjoiMjA5MzkifQ.gU4RmV71ATYmV-gJQPRUfX1A3wHnkmaKRDoH8T44yPE",
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded"
     }
}

```
