### Ejemplo de Recuperación de documentos de una póliza

De manera habitual, los documentos que podremos consultar de una póliza son:

* Condicionado General
* Resumen

Los pasos a seguir serán:
* 1.- [Inicio de sesión](../methods/login.md)
* 2.- [Recuperar listado de pólizas contratables](../methods/listado_polizas_contratables.md)
* 3.- [Descarga de documentos](../methods/recuperar_documentos.md)


****
### 1.- Inicio de Sesión
Realizamos la llamada de inicio de sesión. Si el resultado es correcto nos devolverá un objeto con información sobre el usuario y la sesión que se inicia, destacando las siguientes:

* RESULT->CONTENT->ID => Código de identificación del usuario.
* RESULT->CONTENT->SESSIONKEY => Token que necesitaremos en las posteriores llamadas como cabecera "AuthToken".


****
**Datos de la solicitud**

```json
{  
  "url": "https://api.intermundial.com/v1/es/auth.json",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",    
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "user": "sandbox",
    "pass": "sandbox_intermundial",
  }
}
```

****
**Respuesta con éxito**

```json
{
  "HTTP_STATUS": 200,
  "CODE": "OK",
  "MESSAGE": "OK",
  "RESPONSE_FORMAT": "JSON",
  "RESULT": {
    "CONTENT": {
--->  "ID": "16",
      "USER": "sandbox",      
      "LASTLOGINDATE": "31/08/2016 16:11:00",
      "MODIFIEDDATE": null,
      "CREATEDDATE": null,
      "STATUS": 1,
      "DELETED": 0,
      "ROLES": "1,3,4,5",
      "ACCESSIBLERESOURCES": [
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::postAuthAction",
        "InterMundial\\WebServiceBundle\\Controller\\Api\\AuthController::deleteAuthAction",
      ],
      "SESSIONKEYVALIDTIME": 1472654460,
--->  "SESSIONKEY": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
      "ENVIROMENT": "prod",
      "USERTYPE": "partner",
      "IDCLIENTE": "null"
    }
  }
}
```


****
### 2.- Recuperar listado de pólizas contratables

Una vez con la sesión iniciada, realizamos otra llamada para recuperar las pólizas que nuestro usuario puede contratar.


****
**Datos de la solicitud**

```
{
  "url": "https://secure-aleon.intermundial.com/api/v1/es/users.json/availablepolicies",
  "method": "GET",
  "headers": {
    "cache-control": "no-cache",
    "content-type": "application/x-www-form-urlencoded",
    "AuthToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj"
  }
}
```


****
**Respuesta con éxito**

Podemos observar que cada póliza nos devuelve un objeto `DOCUMENTOS`, destacando los siguientes atributos:

* RESULT->CONTENT[0]->DOCUMENTOS => Listado de documentos de la póliza
* RESULT->CONTENT[0]->DOCUMENTOS[0]->IDDOCUMENTO => Identificador único del documento. Se utilizara en la llamada de [Recuperación de documentos](../methods/recuperar_documentos.md)

Es importante fijarse en `RESULT->CONTENT[0]->DOCUMENTOS[0]->DESCRIPCION`

* Si su valor es `Condicionado` se tratara del condicionado general del seguro.
* Si su valor es `Resumen` se tratara del resumen de las condiciones de la póliza.

```json
{
  HTTP_STATUS: 200,
  CODE: "OK",
  MESSAGE: "OK",
  RESPONSE_FORMAT: "JSON",
  RESULT: {
    CONTENT: [
      {
        IDPOLIZA: "15800",
        NUMPOLIZA: "MMC-PRUEBA8",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "775",
        TARIFADESC: "PRUEBA PRIMAFIJA",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4781",
        PRODUCTOCIA: "PRUEBA PRIMA FIJA",
        LEYENDALISTADOS: "PRUEBA PRIMA FIJA",
        LEYENDACOMERCIAL: "PRUEBA PRIMA FIJA",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789049",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789048",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15799",
        NUMPOLIZA: "MMC-PRUEBA7",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "774",
        TARIFADESC: "PRUEBA DURACION/EDAD",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4780",
        PRODUCTOCIA: "PRUEBA DURACION/EDAD",
        LEYENDALISTADOS: "PRUEBA DURACION/EDAD",
        LEYENDACOMERCIAL: "PRUEBA DURACION/EDAD",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789047",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789046",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15465",
        NUMPOLIZA: "MMC-PRUEBA6",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "773",
        TARIFADESC: "PRUEBA DURACION/IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4779",
        PRODUCTOCIA: "PRUEBA DURACION/IMPORTE",
        LEYENDALISTADOS: "PRUEBA DURACION/IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA DURACION/IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789045",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789043",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15464",
        NUMPOLIZA: "MMC-PRUEBA5",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "772",
        TARIFADESC: "PRUEBA DESTINO/IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4704",
        PRODUCTOCIA: "PRUEBA DESTINO/IMPORTE",
        LEYENDALISTADOS: "PRUEBA DESTINO/IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA DESTINO/IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789042",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789041",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15463",
        NUMPOLIZA: "MMC-PRUEBA4",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "771",
        TARIFADESC: "PRUEBA IMPORTE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4703",
        PRODUCTOCIA: "PRUEBA IMPORTE",
        LEYENDALISTADOS: "PRUEBA IMPORTE",
        LEYENDACOMERCIAL: "PRUEBA IMPORTE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789040",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789039",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15348",
        NUMPOLIZA: "MMC-PRUEBA3",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "622",
        TARIFADESC: "PRUEBA PORCENTAJE",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "512",
        RAMO: "ANULACION MINORISTA",
        RAMOCLIENTE: "ANULACION MINORISTA",
        IDPRODUCTOCIA: "4683",
        PRODUCTOCIA: "PRUEBA PORCENTAJE",
        LEYENDALISTADOS: "PRUEBA PORCENTAJE",
        LEYENDACOMERCIAL: "PRUEBA PORCENTAJE",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789038",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789037",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15347",
        NUMPOLIZA: "MMC-PRUEBA2",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "621",
        TARIFADESC: "PRUEBA CANCELACION",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "512",
        RAMO: "ANULACION MINORISTA",
        RAMOCLIENTE: "ANULACION MINORISTA",
        IDPRODUCTOCIA: "4684",
        PRODUCTOCIA: "PRUEBA CANCELACION",
        LEYENDALISTADOS: "PRUEBA CANCELACION",
        LEYENDACOMERCIAL: "PRUEBA CANCELACION",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789035",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789033",
            NOMBRE: "COND_GEN_SEG_TOTALTRAVEL_MMC-970015.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general del seguro",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      },
      {
        IDPOLIZA: "15319",
        NUMPOLIZA: "MMC-PRUEBA",
        CLASEPOLIZA: "VIDA-ACCIDENTE",
        IDTARIFA: "601",
        TARIFADESC: "PRUEBA",
        IDAREA: "49",
        AREA: "MINORISTAS",
        IDRAMO: "514",
        RAMO: "ASISTENCIA OPCIONAL MINORISTA",
        RAMOCLIENTE: "ASISTENCIA OPCIONAL MINORISTA",
        IDPRODUCTOCIA: "4677",
        PRODUCTOCIA: "PRUEBA",
        LEYENDALISTADOS: "PRUEBA",
        LEYENDACOMERCIAL: "PRUEBA",
        IDCIA: "130",
        NOMBRECIA: "PRUEBA",
        RIESGODESC: "PRUEBA",
        DOCUMENTOS: [
          {
            IDDOCUMENTO: "2789031",
            NOMBRE: "RESUMEN_TOTALTRAVEL_MMC-970015_2015.pdf",
            DESCRIPCION: "Resumen",
            OBSERVACIONES: "Resumen de las condiciones de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          },
          {
            IDDOCUMENTO: "2789029",
            NOMBRE: "COND_GEN_SEG_TOTALSPORTS_MMC-970033.pdf",
            DESCRIPCION: "Condicionado",
            OBSERVACIONES: "Condicionado general de la póliza",
            URL: null,
            FECHAALTA: "23/11/2016",
            TEMPORAL: "0"
          }
        ]
      }
    ]
  }
}
```


****
### 3.- Descarga de documentos

Una vez tenemos el ID del Documento, lanzamos la última llamada para decargarlo.


****
**Datos de la solicitud**

```json
{   "url": "https://api.intermundial.com/v1/es/insurances.json/documents/2788089",
    "method": "GET",
    "headers": {
        "AuthToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NzI2NTQ0NjAsInN1YiI6IklkZW50aWZpY2Fkb3IgZGUgc2Vzc2lvbiIsInNlc3Npb25JZCI6InAzYnRodmlybTlzcHM4OHBldm9mMDdtdDE1IiwidXNlcklkIjoiMjIifQ.452TuLWKJoHs2osoBxPozOGP6lsVNBUx96c2TPWj",
        "cache-control": "no-cache",
        "content-type": "application/x-www-form-urlencoded",
     }
}
```
